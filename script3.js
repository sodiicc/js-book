// test: no

// (function () {
//     "use strict"

//     let active = null

//     const places = {
//         "Alice's House": { x: 279, y: 100 },
//         "Bob's House": { x: 295, y: 203 },
//         "Cabin": { x: 372, y: 67 },
//         "Daria's House": { x: 183, y: 285 },
//         "Ernie's House": { x: 50, y: 283 },
//         "Farm": { x: 36, y: 118 },
//         "Grete's House": { x: 35, y: 187 },
//         "Marketplace": { x: 162, y: 110 },
//         "Post Office": { x: 205, y: 57 },
//         "Shop": { x: 137, y: 212 },
//         "Town Hall": { x: 202, y: 213 }
//     }
//     const placeKeys = Object.keys(places)

//     const speed = 2

//     class Animation {
//         constructor(worldState, robot, robotState) {
//             this.worldState = worldState
//             this.robot = robot
//             this.robotState = robotState
//             this.turn = 0

//             let outer = (window.__sandbox ? window.__sandbox.output.div : document.body), doc = outer.ownerDocument
//             this.node = outer.appendChild(doc.createElement("div"))
//             this.node.style.cssText = "position: relative; line-height: 0.1; margin-left: 10px"
//             this.map = this.node.appendChild(doc.createElement("img"))
//             this.map.src = "img/village2x.png"
//             this.map.style.cssText = "vertical-align: -8px"
//             this.robotElt = this.node.appendChild(doc.createElement("div"))
//             this.robotElt.style.cssText = `position: absolute; transition: left ${0.8 / speed}s, top ${0.8 / speed}s;`
//             let robotPic = this.robotElt.appendChild(doc.createElement("img"))
//             robotPic.src = "img/robot_idle2x.png"
//             this.parcels = []

//             this.text = this.node.appendChild(doc.createElement("span"))
//             this.button = this.node.appendChild(doc.createElement("button"))
//             this.button.style.cssText = "color: white; background: #28b; border: none; border-radius: 2px; padding: 2px 5px; line-height: 1.1; font-family: sans-serif; font-size: 80%"
//             this.button.textContent = "Stop"

//             this.button.addEventListener("click", () => this.clicked())
//             this.schedule()

//             this.updateView()
//             this.updateParcels()

//             this.robotElt.addEventListener("transitionend", () => this.updateParcels())
//         }


//         updateView() {
//             let pos = places[this.worldState.place]
//             this.robotElt.style.top = (pos.y - 38) + "px"
//             this.robotElt.style.left = (pos.x - 16) + "px"

//             this.text.textContent = ` Turn ${this.turn} `
//         }

//         updateParcels() {
//             while (this.parcels.length) this.parcels.pop().remove()
//             let heights = {}
//             for (let { place, address } of this.worldState.parcels) {
//                 let height = heights[place] || (heights[place] = 0)
//                 heights[place] += 14
//                 let node = document.createElement("div")
//                 let offset = placeKeys.indexOf(address) * 16
//                 node.style.cssText = "position: absolute; height: 16px; width: 16px; background-image: url(img/parcel2x.png); background-position: 0 -" + offset + "px";
//                 if (place == this.worldState.place) {
//                     node.style.left = "25px"
//                     node.style.bottom = (20 + height) + "px"
//                     this.robotElt.appendChild(node)
//                 } else {
//                     let pos = places[place]
//                     node.style.left = (pos.x - 5) + "px"
//                     node.style.top = (pos.y - 10 - height) + "px"
//                     this.node.appendChild(node)
//                 }
//                 this.parcels.push(node)
//             }
//         }

//         tick() {
//             let { direction, memory } = this.robot(this.worldState, this.robotState)
//             this.worldState = this.worldState.move(direction)
//             this.robotState = memory
//             this.turn++
//             this.updateView()
//             if (this.worldState.parcels.length == 0) {
//                 this.button.remove()
//                 this.text.textContent = ` Finished after ${this.turn} turns`
//                 this.robotElt.firstChild.src = "img/robot_idle2x.png"
//             } else {
//                 this.schedule()
//             }
//         }

//         schedule() {
//             this.timeout = setTimeout(() => this.tick(), 1000 / speed)
//         }

//         clicked() {
//             if (this.timeout == null) {
//                 this.schedule()
//                 this.button.textContent = "Stop"
//                 this.robotElt.firstChild.src = "img/robot_idle2x.png"
//             } else {
//                 clearTimeout(this.timeout)
//                 this.timeout = null
//                 this.button.textContent = "Start"
//                 this.robotElt.firstChild.src = "img/robot_idle2x.png"
//             }
//         }
//     }

//     window.runRobotAnimation = function (worldState, robot, robotState) {
//         if (active && active.timeout != null)
//             clearTimeout(active.timeout)
//         active = new Animation(worldState, robot, robotState)
//     }
// })()

// var roads = [
//     "Alice's House-Bob's House", "Alice's House-Cabin",
//     "Alice's House-Post Office", "Bob's House-Town Hall",
//     "Daria's House-Ernie's House", "Daria's House-Town Hall",
//     "Ernie's House-Grete's House", "Grete's House-Farm",
//     "Grete's House-Shop", "Marketplace-Farm",
//     "Marketplace-Post Office", "Marketplace-Shop",
//     "Marketplace-Town Hall", "Shop-Town Hall"
// ];

// function buildGraph(edges) {
//     let graph = Object.create(null);
//     function addEdge(from, to) {
//         if (graph[from] == null) {
//             graph[from] = [to];
//         } else {
//             graph[from].push(to);
//         }
//     }
//     for (let [from, to] of edges.map(r => r.split("-"))) {
//         addEdge(from, to);
//         addEdge(to, from);
//     }
//     return graph;
// }

// var roadGraph = buildGraph(roads);

// var VillageState = class VillageState {
//     constructor(place, parcels) {
//         this.place = place;
//         this.parcels = parcels;
//     }

//     move(destination) {
//         if (!roadGraph[this.place].includes(destination)) {
//             return this;
//         } else {
//             let parcels = this.parcels.map(p => {
//                 if (p.place != this.place) return p;
//                 return { place: destination, address: p.address };
//             }).filter(p => p.place != p.address);
//             return new VillageState(destination, parcels);
//         }
//     }
// }

// function runRobot(state, robot, memory) {
//     for (let turn = 0; ; turn++) {
//         if (state.parcels.length == 0) {
//             console.log(`Done in ${turn} turns`);
//             break;
//         }
//         let action = robot(state, memory);
//         state = state.move(action.direction);
//         memory = action.memory;
//         console.log(`Moved to ${action.direction}`);
//     }
// }

// function randomPick(array) {
//     let choice = Math.floor(Math.random() * array.length);
//     return array[choice];
// }

// function randomRobot(state) {
//     return { direction: randomPick(roadGraph[state.place]) };
// }

// VillageState.random = function (parcelCount = 5) {
//     let parcels = [];
//     for (let i = 0; i < parcelCount; i++) {
//         let address = randomPick(Object.keys(roadGraph));
//         let place;
//         do {
//             place = randomPick(Object.keys(roadGraph));
//         } while (place == address);
//         parcels.push({ place, address });
//     }
//     return new VillageState("Post Office", parcels);
// };

// var mailRoute = [
//     "Alice's House", "Cabin", "Alice's House", "Bob's House",
//     "Town Hall", "Daria's House", "Ernie's House",
//     "Grete's House", "Shop", "Grete's House", "Farm",
//     "Marketplace", "Post Office"
// ];

// function routeRobot(state, memory) {
//     if (memory.length == 0) {
//         memory = mailRoute;
//     }
//     return { direction: memory[0], memory: memory.slice(1) };
// }

// function findRoute(graph, from, to) {
//     let work = [{ at: from, route: [] }];
//     for (let i = 0; i < work.length; i++) {
//         let { at, route } = work[i];
//         for (let place of graph[at]) {
//             if (place == to) return route.concat(place);
//             if (!work.some(w => w.at == place)) {
//                 work.push({ at: place, route: route.concat(place) });
//             }
//         }
//     }
// }

// function goalOrientedRobot({ place, parcels }, route) {
//     if (route.length == 0) {
//         let parcel = parcels[0];
//         if (parcel.place != place) {
//             route = findRoute(roadGraph, place, parcel.place);
//         } else {
//             route = findRoute(roadGraph, place, parcel.address);
//         }
//     }
//     return { direction: route[0], memory: route.slice(1) };
// }

// function countSteps(state, robot, memory) {
//     for (let steps = 0; ; steps++) {
//         if (state.parcels.length == 0) return steps;
//         let action = robot(state, memory);
//         state = state.move(action.direction);
//         memory = action.memory;
//     }
// }

// function lazyRobot({ place, parcels }, route) {
//     if (route.length == 0) {
//         // Describe a route for every parcel
//         let routes = parcels.map(parcel => {
//             if (parcel.place != place) {
//                 return {
//                     route: findRoute(roadGraph, place, parcel.place),
//                     pickUp: true
//                 };
//             } else {
//                 return {
//                     route: findRoute(roadGraph, place, parcel.address),
//                     pickUp: false
//                 };
//             }
//         });

//         // This determines the precedence a route gets when choosing.
//         // Route length counts negatively, routes that pick up a package
//         // get a small bonus.
//         function score({ route, pickUp }) {
//             return (pickUp ? 0.5 : 0) - route.length;
//         }
//         route = routes.reduce((a, b) => score(a) > score(b) ? a : b).route;
//     }

//     return { direction: route[0], memory: route.slice(1) };
// }

// function compareRobots(robot1, memory1, robot2, memory2) {
//     let total1 = 0, total2 = 0;
//     for (let i = 0; i < 1000; i++) {
//         let state = VillageState.random();
//         total1 += countSteps(state, robot1, memory1);
//         total2 += countSteps(state, robot2, memory2);
//     }
//     console.log(`Robot 1 needed ${total1 / 1000} steps per task`)
//     console.log(`Robot 2 needed ${total2 / 1000}`)
// }

// compareRobots(lazyRobot, [], goalOrientedRobot, []);

// runRobotAnimation(VillageState.random(), lazyRobot, [])

// console.log('[1,2,3,4,5].slice(1)', [1, 2, 3, 4, 5].slice(3))

// class PGroup {
//     constructor(arr) {
//         this.arr = arr
//     }

//     add(val) {
//         if (this.has(val)) return this
//         return new PGroup(this.arr.concat([val]))
//     }
//     has(val) {
//         return this.arr.includes(val)
//     }
//     delete(val) {
//         if (!this.has(val)) return this
//         return new PGroup(this.arr.filter(el => el !== val))
//     }
// }

// PGroup.empty = new PGroup([]);

// let a = PGroup.empty.add("a");
// let ab = a.add("b");
// let b = ab.delete("a");

// console.log(b.has("b"));
// console.log(a.has("b"));
// console.log(b.has("a"))

// function numberToString(n, base = 10) {
//     let result = "", sign = "";
//     if (n < 0) {
//         sign = "-";
//         n = -n;
//     }
//     do {
//         result = String(n % base) + result;
//         console.log('result', result)
//         n = Math.floor(n / base);
//     } while (n > 0);
//     return sign + result;
// }
// console.log(numberToString(31, 16))

// function promptDirection(question) {
//     let result = prompt(question)
//     if (result.toLowerCase() == "left") return "L"
//     if (result.toLowerCase() == "right") return "R"
//     throw new Error("Неверное направление: " + result)
// }
// function look() {
//     if (promptDirection("Куда двигаться?") == "L") {
//         return "дом"
//     } else {
//         return "два злых медведя";
//     }
// }
// try {
//     console.log("Перед вами", look())
// } catch (error) {
//     console.log("Что-то пошло не так: " + error)
// }

// class multUnitFailure extends Error {}
// let res 

// const mult = () => {
//     if(Math.random() > 0.8) {
//         res = 8*9
//         return 8*9
//     } 
//     res = null    
// }
// const getRes = () => {
//     try{
//             console.log(mult())
//             if(!res)  throw new multUnitFailure('System Error')


//     }catch(e){
//         console.log('Системна помилка:' + e)
//     }
// }

// while(!res) getRes()

// class MultiplicatorUnitFailure extends Error {}

// function primitiveMultiply(a, b) {
//   if (Math.random() < 0.1) {
//     return a * b;
//   } else {
//     throw new MultiplicatorUnitFailure("Klunk");
//   }
// }

// function reliableMultiply(a, b) {
//   for (;;) {
//     try {
//       return primitiveMultiply(a, b);
//     } catch (e) {
//       console.log('e', e)
//       if (!(e instanceof MultiplicatorUnitFailure))
//         throw e;
//     }
//   }
// }

// console.log(reliableMultiply(8, 8));

// const box = {
//     locked: true,
//     unlock() { this.locked = false; },
//     lock() { this.locked = true; },
//     _content: [],
//     get content() {
//     if (this.locked) throw new Error("Заперто!");
//     return this._content;
//     }
//    }

//    function withBoxUnlocked(body) {
//     let locked = box.locked;
//     if (!locked) {
//       return body();
//     }

//     box.unlock();
//     try {
//       return body();
//     } finally {
//       box.lock();
//     }
//   }
//   withBoxUnlocked(function() {
//       box.content.push("gold piece");
//     });

//     try {
//         box.unlock()
//     withBoxUnlocked(function() {
//       throw new Error("Pirates on the horizon! Abort!");
//     });
//   } catch (e) {
//     console.log("Error raised: " + e);
//   }
//   console.log(box.locked);

// console.log(/^(\d{1,2})-(\d{1,2})-(\d{4})$/.exec('1-1-3005'))

// function getDate(string) {
//   let date = /^(\d{1,5})-(\d{1,5})-(\d{4})$/.exec(string)
//   if (date) {
//     let [_, day, month, year] = date;
//     return new Date(year, month - 1, day);
//   }
//   return ('Enter correct date pls !')
// }
// console.log(getDate("100-1-2000"));

// console.log(/\bcat\b/.test("cat"))

// console.log(/\b\d+ (pig|cow|chicken)s?\b/.test("tyuy 15 pigs"))
// console.log(/([01]+)+b/.test('1101010'))

// console.log(
//   "Liskov, Barbara McCarthy, John Wadler, Philip"
//     .replace(/(\w+), (\w+)/g, "$2 sraka $1 $2 $2,"));

// let stock = "10 lemons, 26 cabbages,  1086 eggs and 34 fuckers";
// function minusOne(match, amount, unit) {
//   console.log('unit', unit)
//   console.log('amount', amount)
//   amount = +amount - 1;
//   if (amount == 1) { // остался только один, убрать 's'
//     unit = unit.slice(0, unit.length - 1);
//   } else if (amount == 0) {
//     amount = "no";
//   }
//   return amount + " " + unit;
// }
// console.log(stock.replace(/(\d+) (\w+)/g, minusOne))

// let global = /abc/;
// console.log(global.exec("jty abc h sth srthrthrt rt  abc h abc"));

// let digit = /\d/g;
// console.log(digit.exec("вот единица: 1"));
// console.log(digit.exec("а теперь: 1"))
// console.log("Banana".match(/an/g))

// let input = "Строка с 3 числами... 42 и 88.";
// let number = /\b\d+\b/g;
// console.log('number.exec(input)', number.exec(input))
// console.log('number.exec(input)', number.exec(input))
// console.log('number.exec(input)', number.exec(input))

// while (match = number.exec(input)) {
//   console.log("Найдено число", match[0], "в позиции", match.index);
// }

// function verify(regexp, yes, no) {
//   if (regexp.source == "...") return;
//   for (let str of yes) if (!regexp.test(str)) {
//     console.log(`Failure to match '${str}'`);
//   } else console.log("OK")
//   for (let str of no) if (regexp.test(str)) {
//     console.log(`Unexpected match for '${str}'`);
//   } else console.log("OK")
// }

//  let reg1 = /ca[rt]/
//  let reg1 = /(p|pr)op/
//  let reg1 = /pr?op/
//  let reg1 = /ferr(et|y|ari)/
//  let reg1 = /ious\b/
//  let reg1 = /\s(\.|\,|\:|\;)/
//  let reg1 = /\s[.,:;]/
//  let reg1 = /\b\w{7}/
// let reg1 = /\b[^e\s]+\b/i

// verify(reg1,
//   ["red platypus", "wobbling nest"],
//   ["earth bed", "learning ape", "BEET"]);

let str = `Rht 'grtgeertg tyuj ty  e' rehert rtreh'rhh 'hrthertherh' Д’Артаньян`
// console.log('str', str.replace(/(^|\W)'|'(\W|$)/g, '$1"$2'))

let number = /^[\+\-]?(\d+(\.\d*)?|\.\d+)([eE][\+\-]?\d+)?$/i;

// Tests:
for (let str of ["1", "-1", "+15", "1.55", ".5", "5.",
                 "1.3e2", "1E-4", "1e+12"]) {
  if (!number.test(str)) {
    console.log(`Failed to match '${str}'`);
  }
}
for (let str of ["1a", "+-1", "1.2.3", "1+1", "1e4.5",
                 ".5.", "1f5", "."]) {
  if (number.test(str)) {
    console.log(`Incorrectly accepted '${str}'`);
  }
}
